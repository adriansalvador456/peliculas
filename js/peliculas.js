document.addEventListener("DOMContentLoaded", function () {
    const inputNombre = document.querySelector("input[type=text]");
    const btnBuscar = document.querySelector("button");
    const nombrePelicula = document.getElementById("nombrePelicula");
    const añoRealizacion = document.getElementById("AñoRealizacion");
    const actoresPrincipales = document.getElementById("ActoresPrincipales");
    const reseñas = document.getElementById("Reseñas");

    btnBuscar.addEventListener("click", buscarPelicula);

    function buscarPelicula() {
        const titulo = inputNombre.value;

        if (titulo.trim() === "") {
            alert("Por favor, ingrese el título de la película.");
            return;
        }

        const url = `https://www.omdbapi.com/?t=${encodeURIComponent(titulo)}&plot=full&apikey=30063268`;

        fetch(url, { mode: 'cors' })
            .then(response => response.json())
            .then(data => {
                if (data.Error) {
                    alert(data.Error);
                } else if (data.Response === "False") {
                    alert("No se encontró la película. Por favor, ingrese un título más específico.");
                } else if (data.Title.toLowerCase() === titulo.toLowerCase()) {
                    mostrarDetallesPelicula(data.imdbID);
                } else {
                    alert("No se encontró la película. Por favor, ingrese un título más específico.");
                }
            })
            .catch(error => {
                console.error("Error al obtener la información de la película", error);
                alert("Error al obtener la información de la película. Por favor, inténtelo de nuevo.");
            });
    }

    function mostrarDetallesPelicula(imdbID) {
        const url = `https://www.omdbapi.com/?i=${imdbID}&plot=full&apikey=30063268`;

        fetch(url)
            .then(response => response.json())
            .then(data => {
                nombrePelicula.textContent = `Nombre: ${data.Title}`;
                añoRealizacion.textContent = `Año de Realización: ${data.Year}`;
                actoresPrincipales.textContent = `Actores principales: ${data.Actors}`;
                reseñas.textContent = `Reseña: ${data.Plot}`;

                const imagen = document.querySelector("img");
                imagen.src = data.Poster;
                imagen.alt = `Imagen de ${data.Title}`;
            })
            .catch(error => {
                console.error("Error al obtener detalles de la película", error);
                alert("Error al obtener detalles de la película. Por favor, inténtelo de nuevo.");
            });
    }
});
